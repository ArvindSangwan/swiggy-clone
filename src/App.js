import React, { Component } from 'react';
import logo from './logo.svg';
import axios from 'axios';
import './App.css';

class App extends Component {
  constructor(){
    super()
    this.state = {
      restaurantsdata : [],
      dataset : new Map()
    }
  }
  componentDidMount(){
    axios.get('http://cdn.adpushup.com/reactTask.json').then(response=>{
      this.setState(()=>{
        return{restaurantsdata:response.data}
      })
    }
    )
    }
    

  render(){
    const colStle = {
      display: "flex",
            flexDirection: "column",
            marginTop: "25px"
    }

    const rowStyle = {
      display: "flex",
            width: "33.33%",
            marginTop: "12px"
    }

    const outerContainer = {
      padding: "20px 20px 57px",
      border: "1px solid #fff",
      contain: "content"
    }

    const widthWrap = {
      position: "relative",
      width: "254px"
    }

    const imageWrapper = {
    background: "#eef0f5",
    width: "254px",
    height: "160px",
    position: "relative"
    }

    const star = {
    fontSize: "10px",
    marginRight: "4px",
    position: "relative",
    top: "-1px",
    fontFamily: "icomoon",
    speak: "none",
    fontStyle: "normal",
    fontWeight: "400",
    fontVariant: "normal",
    textTransform: "none",
    lineHeight: "1",
    webkitFontSmoothing: "antialiased"
    }

    const footerStarWrap ={
      borderTop: "1px solid #e9e9eb",
    paddingTop: "14px",
    marginTop: "14px",
    color: "#8a584b",
    display: "-ms-flexbox",
    display: "flex",
    alignItems: "center",
    fontWeight: "600"
    }
    let restaurantsdata = this.state.restaurantsdata;
    let rowData = []
    let wrapArray= []
    let headerdata =[]
    let dataset = new Map()
    let menuitems = []
    let screenData=()=>{
      for(let i=0; i<restaurantsdata.length; i++){
        headerdata = []
        headerdata.push(restaurantsdata[i].category)
        for(let j =0; j<restaurantsdata[i].restaurantList.length; j++){
          if(headerdata.length >0 && j===i){
            menuitems.push(<li style={{height:"40px"}}>{headerdata}</li>)
          }
          rowData.push(
            <>
           { headerdata.length >0 && j===i? <h1>{headerdata}</h1> : ""}
            <div style={outerContainer}>
        <div style={widthWrap}>
          <div  style = {imageWrapper}>
  
  <img class="_2tuBw _12_oN" alt="Anshika Jain Bhojnalaya" width="254" height="160" src="https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/hcrvbyxvclaukubuymtq"></img>  
  
          </div>
  <div style={{marginTop: "14px"}}>
  <div class="nA6kb" style={{fontSize: "17px",
      fontWeight: "500",
      wordBreak: "break-word"}}>{restaurantsdata[i].restaurantList[j].name}</div>
  
  
  {/* <div class="_1gURR" title="North Indian, Chinese, Fast Food">{
  for(let k =0;j<restaurantsdata[i].restaurantList[j].food_types.length; j++){
    restaurantsdata[i].restaurantList[j].food_types[k]
    }
  }</div> */}
  
  </div>
  
  <div className = "deliveryDetails" style={{display: "flex", alignIttems: "center", justifyContent: "space-between", color: "#535665", marginTop: "18px",fontSize: "12px"}}>
  <div className ="starDetails" style={{height: "20px",
      width: "43px",
      padding: "0 5px",
      fontWeight: "400"}}>
  <span class="icon-star _537e4" 
  style={star}></span>
  <span>{restaurantsdata[i].restaurantList[j].ratings}</span>
        </div>
        <div>•</div>
          <div>{restaurantsdata[i].restaurantList[j].delivery_time}</div>
        <div>•</div>
        <div class="nVWSi">₹250 FOR TWO</div>
        </div>
        <div style={footerStarWrap}>
        <span class="icon-offer-filled _2fujs" style={{fontSize: "16px",
      width: "20px",
      height: "16px",
      marginRight: "4px"}}></span>
        <span class="sNAfh" style={{fontWeight: "400",
      textOverflow: "ellipsis",
      overflow: "hidden",
      whiteSpace: "nowrap"}}>10% off | Use coupon SWIGGYIT</span>
      </div>
      </div>
      </div>
      </>
          )
        
        if(rowData.length === 3 || j=== restaurantsdata[i].restaurantList.length){
          wrapArray.push(
            <div style={rowStyle}>
              {rowData}
            </div>
          )
          rowData=[]
        }
        
        
        }
        }
    }
    screenData()
    let navigateto = (event)=>{
    //   $('.colStyle').find('h1').click(function(){
    //     var $href = $(this).attr('href');
    //     var $anchor = $('#'+$href).offset();
    //     window.scrollTo($anchor.left,$anchor.top);
    //     return false;
    // });
    }

  return (
    <>
    <div style ={{width: "254px"}}>
      <div style= {{boxShadow: "0 2px 4px 0 rgba(48,56,97,.2)",
    paddingTop: "35px",
    top: "80px",
    hidden:"460px",
    overflow: "hidden"}}>
      <ul style={{listStyle:"none"}}>
        <span onClick = {(e)=>{
          navigateto(e)
        }}>{menuitems}</span>
        </ul>
    </div>
    </div>
    <div style ={colStle}>
    {wrapArray}</div>
    </>
  );
}
}

export default App;
